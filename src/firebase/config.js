export default {
  apiKey: "...",
  authDomain: "instaguestbook.firebaseapp.com",
  databaseURL: "https://instaguestbook.firebaseio.com",
  projectId: "instaguestbook",
  storageBucket: "instaguestbook.appspot.com",
  messagingSenderId: "...",
  appId: "..."
};